appdirs==1.4.4
black==21.6b0
mypy-extensions==0.4.3
pathspec==0.8.1
regex==2021.7.1
