PREFIX ?= $(HOME)/.local
BINDIR ?= $(PREFIX)/bin

CONFIG_DIR ?= $(HOME)/.config/lasso
TEMPLATES_DIR := $(CONFIG_DIR)/templates

install:
	@install -v -d "$(DESTDIR)$(BINDIR)"
	@install -v -m 0644 src/lasso "$(DESTDIR)$(BINDIR)"

	@install -v -d "$(DESTDIR)$(CONFIG_DIR)/templates/default"
	@install -v -m 0644 etc/lasso/templates/default/CONTRIBUTING.md "$(DESTDIR)$(TEMPLATES_DIR)/default"
	@install -v -m 0644 etc/lasso/templates/default/COPYING.md "$(DESTDIR)$(TEMPLATES_DIR)/default"
	@install -v -m 0644 etc/lasso/templates/default/pyproject.toml "$(DESTDIR)$(TEMPLATES_DIR)/default"
	@install -v -m 0644 etc/lasso/templates/default/README.md "$(DESTDIR)$(TEMPLATES_DIR)/default"
	@install -v -m 0644 etc/lasso/templates/default/requirements_dev.txt "$(DESTDIR)$(TEMPLATES_DIR)/default"
	@install -v -m 0644 etc/lasso/templates/default/requirements_test.txt "$(DESTDIR)$(TEMPLATES_DIR)/default"
	@install -v -m 0644 etc/lasso/templates/default/requirements.txt "$(DESTDIR)$(TEMPLATES_DIR)/default"
	@install -v -m 0644 etc/lasso/templates/default/setup.cfg "$(DESTDIR)$(TEMPLATES_DIR)/default"
	@install -v -m 0644 etc/lasso/templates/default/.gitignore "$(DESTDIR)$(TEMPLATES_DIR)/default"

	@install -v -d "$(DESTDIR)$(CONFIG_DIR)/templates/default/src/{{name}}"
	@install -v -m 0644 etc/lasso/templates/default/src/{{name}}/__init__.py "$(DESTDIR)$(TEMPLATES_DIR)/default/src/{{name}}"
	@install -v -m 0644 etc/lasso/templates/default/src/{{name}}/lib.py "$(DESTDIR)$(TEMPLATES_DIR)/default/src/{{name}}"
	@install -v -m 0644 etc/lasso/templates/default/src/{{name}}/__main__.py "$(DESTDIR)$(TEMPLATES_DIR)/default/src/{{name}}"

	@install -v -d "$(DESTDIR)$(CONFIG_DIR)/templates/default/tests"
	@install -v -m 0644 etc/lasso/templates/default/tests/conftest.py "$(DESTDIR)$(TEMPLATES_DIR)/default/tests"
	@install -v -m 0644 etc/lasso/templates/default/tests/fake_data.py "$(DESTDIR)$(TEMPLATES_DIR)/default/tests"
	@install -v -m 0644 etc/lasso/templates/default/tests/__init__.py "$(DESTDIR)$(TEMPLATES_DIR)/default/tests"
	@install -v -m 0644 etc/lasso/templates/default/tests/stubs.py "$(DESTDIR)$(TEMPLATES_DIR)/default/tests"
	@install -v -m 0644 etc/lasso/templates/default/tests/test_lib.py "$(DESTDIR)$(TEMPLATES_DIR)/default/tests"
	@install -v -m 0644 etc/lasso/templates/default/tests/test_main.py "$(DESTDIR)$(TEMPLATES_DIR)/default/tests"
