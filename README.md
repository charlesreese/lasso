# Lasso

Lasso is a command-line tool for effortlessly creating Python projects.

Use Lasso to create Python projects from templates. When generating your project, Lasso creates the virtual environment automatically. Activating is as easy as changing to the project's directory and invoking `lasso shell`. Lasso activates the virtual environment in a subshell. When you're done, just type `exit`.

## Installation

Clone this repository and install Lasso:

```console?prompt=$
$ git clone https://gitlab.com/charlesreese/lasso
$ cd lasso
$ make install
$ source ~/.local/bin/lasso
```

Copy the default project template:

```console?prompt=$
$ cp -a ~/.config/lasso/templates/default/. ~/.config/lasso/templates/my-default
```

Customize it how you like. When generating your project, Lasso substitutes the project's name everywhere it finds `{{name}}`.

Add a line to your `.bashrc` so Lasso is always available:

```console?prompt=$
$ echo "source ~/.local/bin/lasso" >> ~/.bashrc
```

## Usage

### Creating New Projects

Create a new project from a template:

```console?prompt=$
$ lasso new my-project my-default
```

The virtual environment is created automatically. Lasso does a few other things like upgrading pip, setuptools, and wheel, and installing any packages it finds in `requirements_dev.txt` and `requirements_test.txt`.

### Working With Virtual Enviroments

Activate your virtual environment:

```console?prompt=$
$ cd path/to/my-project
$ lasso shell
```

This activates the project's virtual environment in a subshell. When you're done, just type `exit`.

If you don't need a virtual enviroment any more, you can remove it:

```console?prompt=$
$ lasso remove my-project
```

This deletes the virtual environment, but leaves the project alone.

## Contributing

Merge requests are welcome. When creating merge requests, please write
[good commits](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)
and
[sign your work](https://www.kernel.org/doc/html/v4.17/process/submitting-patches.html#sign-your-work-the-developer-s-certificate-of-origin). Contributions are subject to the [Developer Certificate of Origin and GNU General Public License, version 3 later](./CONTRIBUTING.md).

## License

Lasso is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License, version 3 later. You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

Copyright (C) 2021 Charles Reese
